﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TestWCFApp.Context;
using TestWCFApp.Models;

namespace TestWCFApp
{
    public class Service1 : IService1
    {
        public IEnumerable<Message> GetLastMessages(int idLastMessage)
        {
            using (var context = new MessageContext())
            {
                return context.Messages.Where(x => x.Id > idLastMessage).OrderBy(x => x.Id).ToList();
            }
        }

        public string Ping()
        {
            return "Pong";
        }

        public int SendMessage(string message, string login)
        {
            using (var context = new MessageContext())
            {
                var messageObject = new Message
                {
                    Date = DateTime.Now,
                    Login = login,
                    Text = message
                };

                context.Messages.Add(messageObject);
                context.SaveChanges();

                return messageObject.Id;
            }
        }
    }
}
