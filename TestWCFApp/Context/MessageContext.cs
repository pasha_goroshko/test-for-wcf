﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestWCFApp.Models;

namespace TestWCFApp.Context
{
    public class MessageContext : DbContext
    {
        public MessageContext():base("MessageDbContext")
        {

        }

        public virtual DbSet<Message> Messages { get; set; }
    }
}