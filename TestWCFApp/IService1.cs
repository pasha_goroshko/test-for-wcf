﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TestWCFApp.Models;

namespace TestWCFApp
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string Ping();

        [OperationContract]
        int SendMessage(string message, string login);

        [OperationContract]
        IEnumerable<Message> GetLastMessages(int idLastMessage);
    }
}
